# Deploy to Azure Kubernetes Service
# Build and push image to Azure Container Registry; Deploy to Azure Kubernetes Service
# https://docs.microsoft.com/azure/devops/pipelines/languages/docker

trigger:
  - develop

resources:
  - repo: self

variables:

  # Container registry service connection established during pipeline creation
  dockerRegistryServiceConnection: 'ec67f6c3-2f43-4999-b1fb-2084c01af4a0'
  imageRepositoryApi: 'benjamin-mathias-api-sender'
  imageRepositoryDb: 'mongodb'
  containerRegistry: 'newcustomacrbe888c7098df4f35a2097088633075c7.azurecr.io'
  tag: '$(Build.BuildId)'
  imagePullSecret: 'newcustomacrbe888c7098df4f35a2097088633075c76701fb11-auth'
  dockerfilePathApi: '**/api/Dockerfile'
  dockerfilePathDb: '**/db/Dockerfile'

  # Agent VM image name
  vmImageName: 'ubuntu-latest'

stages:
  - stage: Lint
    displayName: Lint stage
    jobs:
      - job: Lint
        pool:
          vmImage: 'ubuntu-latest'
        steps:
          - script: docker pull github/super-linter:slim
            displayName: Pull GitHub Super-Linter image
          - script: >-
              docker run \
                -e RUN_LOCAL=true \
                -e VALIDATE_BASH=false \
                -e VALIDATE_PYTHON_MYPY=false \
                -e VALIDATE_PYTHON_ISORT=false \
                -e VALIDATE_MARKDOWN=false \
                -e VALIDATE_DOCKERFILE_HADOLINT=false \
                -v $(System.DefaultWorkingDirectory):/tmp/lint \
                github/super-linter
            displayName: 'Run GitHub Super-Linter'

  - stage: Test
    displayName: Test stage
    jobs:
      - job: Unit_test
        pool:
          vmImage: 'ubuntu-latest'
        strategy:
          matrix:
            Python39:
              python.version: '3.9'
        steps:
          - task: UsePythonVersion@0
            displayName: 'Use Python $(python.version)'
            inputs:
              versionSpec: '$(python.version)'
          - script: |
              pip install pytest pytest-azurepipelines
              pip install pytest-cov
              pip install -r ./api/src/requirements.txt
              pytest ./api/tests/unit --doctest-modules --junitxml=junit/test-results.xml --cov=./api/src --cov-report=xml
            displayName: 'Unit tests'
          - task: PublishTestResults@2
            condition: succeededOrFailed()
            inputs:
              testResultsFiles: '**/test-*.xml'
              testRunTitle: 'Publish test results for Python $(python.version)'
          - task: PublishCodeCoverageResults@1
            inputs:
              codeCoverageTool: Cobertura
              summaryFileLocation: '$(System.DefaultWorkingDirectory)/**/coverage.xml'

      - job: Functional_test
        pool:
          vmImage: 'ubuntu-latest'
        strategy:
          matrix:
            Python39:
              python.version: '3.9'
        steps:
          - task: UsePythonVersion@0
            displayName: 'Use Python $(python.version)'
            inputs:
              versionSpec: '$(python.version)'
          - script: |
              pip install pytest pytest-azurepipelines
              pip install -r ./api/src/requirements.txt
              pytest ./api/tests/functional
            displayName: 'Functional tests'

  - stage: Build
    displayName: Build stage
    jobs:
      - job: Build
        displayName: Build
        pool:
          vmImage: $(vmImageName)
        steps:
          - task: Docker@2
            displayName: Build and push the api image to container registry
            inputs:
              command: buildAndPush
              repository: $(imageRepositoryApi)
              dockerfile: $(dockerfilePathApi)
              containerRegistry: $(dockerRegistryServiceConnection)
              tags: v1.0.0
          - task: Docker@2
            displayName: Build and push the db image to container registry
            inputs:
              command: buildAndPush
              repository: $(imageRepositoryDb)
              dockerfile: $(dockerfilePathDb)
              containerRegistry: $(dockerRegistryServiceConnection)
              tags: v1.0.0
          
          - script: |
              echo -e "MONGODB_USER=$(MONGODB_USER)" >> .env
              echo -e "MONGO_INITDB_ROOT_USERNAME=$(MONGO_INITDB_ROOT_USERNAME)" >> .env
            displayName: 'Write .env'
          - task: Kubernetes@1
            displayName: Create configMap and apply secret
            inputs:
              connectionType: 'Kubernetes Service Connection'
              kubernetesServiceEndpoint: 'benjamin-mathiasCICDDemo-defaultAks-default-1630248118005'
              secretType: generic
              secretArguments: >-
                --from-literal=MONGODB_PASSWORD=$(MONGODB_PASSWORD)
                --from-literal=MONGO_INITDB_ROOT_PASSWORD=$(MONGO_INITDB_ROOT_PASSWORD)
              secretName: mongosecret
              configMapName: core-config
              forceUpdateConfigMap: true
              configMapArguments: --from-env-file=.env
            env:
              MONGODB_PASSWORD: $(MONGODB_PASSWORD) 
              MONGO_INITDB_ROOT_PASSWORD: $(MONGO_INITDB_ROOT_PASSWORD) 

          - publish: manifests
            artifact: manifests

  - stage: Deploy
    displayName: Deploy stage
    dependsOn: Build

    jobs:
      - deployment: Deploy
        displayName: Deploy
        pool:
          vmImage: $(vmImageName)
        environment: 'benjamin-mathiasCICDDemo-7555.default'
        strategy:
          runOnce:
            deploy:
              steps:
                - task: KubernetesManifest@0
                  displayName: Create imagePullSecret
                  inputs:
                    action: createSecret
                    secretName: $(imagePullSecret)
                    dockerRegistryEndpoint: $(dockerRegistryServiceConnection)

                - task: KubernetesManifest@0
                  displayName: Deploy to Kubernetes cluster
                  inputs:
                    action: deploy
                    manifests: |
                      $(Pipeline.Workspace)/manifests/deployment.yml
                      $(Pipeline.Workspace)/manifests/service.yml
                    imagePullSecrets: |
                      $(imagePullSecret)
                    containers: |
                      $(containerRegistry)/$(imageRepositoryApi):v1.0.0
                      $(containerRegistry)/$(imageRepositoryDb):v1.0.0

  - stage: Integration_test
    displayName: Integration test stage
    jobs:
      - job: Integration_test
        pool:
          vmImage: 'ubuntu-latest'
        strategy:
          matrix:
            Python39:
              python.version: '3.9'
        steps:
          - task: UsePythonVersion@0
            displayName: 'Use Python $(python.version)'
            inputs:
              versionSpec: '$(python.version)'
          - script: |
              pip install pytest pytest-azurepipelines
              pip install -r ./api/src/requirements.txt
              response=$(curl --write-out '%{http_code}' --silent --output /dev/null https://datas.to/api/v1/status)
              [[ "${response}" == "200" ]] || sleep 20
              pytest ./api/tests/integration
            displayName: 'Integration tests'
