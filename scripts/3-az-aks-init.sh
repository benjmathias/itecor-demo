#!/usr/bin/env bash

# Example 1 : bash 3-az-aks-init.sh
# Example 2 : bash 3-az-aks-init.sh myContainerRegistryUniqueName myNewResourceGroup

# Optional arguments are :
# $1 => aks cluster name, if unset use $DEFAULT_AKS_CLUSTER_NAME
# $2 => node count, if unset use $DEFAULT_NODE_COUNT

set -e
trap 'echo "Exiting script due to fatal error... Exit code was $?."' ERR

function load_env_from_file {
  set -o allexport
  # shellcheck source=/tmp/az_scripts_vars.env
  source "${1}"
  set +o allexport
}

DEFAULT_AKS_CLUSTER_NAME=defaultAks
DEFAULT_NODE_COUNT=1
AKS_CLUSTER_NAME="${1:-$DEFAULT_AKS_CLUSTER_NAME}"
NODE_COUNT="${2:-$DEFAULT_NODE_COUNT}"
VARS_FILE_PATH=${VARS_FILE_PATH:-"/tmp/az_scripts_vars.env"}

echo -e "\n### Loading vars from tmp file ${VARS_FILE_PATH}\n"
load_env_from_file "${VARS_FILE_PATH}"

echo -e "\n### Showing loaded vars and overwriting var file in ${VARS_FILE_PATH}...\n"
: >"${VARS_FILE_PATH}"
echo "ACR_NAME=${ACR_NAME}" | tee -a "${VARS_FILE_PATH}"
echo "RESOURCE_GROUP=${RESOURCE_GROUP}" | tee -a "${VARS_FILE_PATH}"
echo "ACR_LOGIN_SERVER=${ACR_LOGIN_SERVER}" | tee -a "${VARS_FILE_PATH}"
echo -e "AKS_CLUSTER_NAME=${AKS_CLUSTER_NAME}\n" | tee -a "${VARS_FILE_PATH}"

echo -e "\n### Reloading vars from tmp file ${VARS_FILE_PATH}\n"
load_env_from_file "${VARS_FILE_PATH}"
echo "Reloading was successful"

####### Necessary if rights are not sufficient
## Get the id of the service principal configured for AKS,
## if not add one (https://docs.microsoft.com/en-us/azure/aks/kubernetes-service-principal?tabs=azure-cli#manually-create-a-service-principal)
#CLIENT_ID=$(az aks show --resource-group $AKS_RESOURCE_GROUP --name $AKS_CLUSTER_NAME --query "servicePrincipalProfile.clientId" --output tsv)
## Get the ACR registry resource id
#ACR_ID=$(az acr show --name $ACR_NAME --resource-group $ACR_RESOURCE_GROUP --query "id" --output tsv)
## Create role assignment
#az role assignment create --assignee $CLIENT_ID --role acrpull --scope $ACR_ID

echo -e "\n### Creating AKS cluster ${AKS_CLUSTER_NAME}\n"

ATTACH_OPTION=( --attach-acr "${ACR_NAME}" )
if [[ -n $NO_ACR_ATTACH ]]; then
  unset ATTACH_OPTION
  echo successful unset
fi
az aks create \
  --resource-group "${RESOURCE_GROUP}" \
  --name "${AKS_CLUSTER_NAME}" \
  --node-count "${NODE_COUNT}" \
  --generate-ssh-keys \
  "${ATTACH_OPTION[@]}"

echo -e "\n### Connect to AKS cluster ${AKS_CLUSTER_NAME}\n"
az aks get-credentials -g "${RESOURCE_GROUP}" -n "${AKS_CLUSTER_NAME}"
