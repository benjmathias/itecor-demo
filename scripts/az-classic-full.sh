#!/usr/bin/env bash

unset NO_ACR_ATTACH

az account show 2> /dev/null
retVal=$?
if [[ ! $retVal -eq 0 ]];then
  echo -e "\n###### az-login.sh \n"
  ./1-az-login.sh
fi

echo -e "\n###### az-acr-init.sh \n"
./2-az-acr-init.sh "newCustomAcr$(uuidgen | tr -d '-')" "newTestGroup$(uuidgen | tr -d '-')"
echo -e "\n###### az-aks-init.sh \n"
./3-az-aks-init.sh
