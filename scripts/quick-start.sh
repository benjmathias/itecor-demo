#!/usr/bin/env bash

### Usage examples :
# ./quick-start.sh
# ./quick-start.sh --build
# ./quick-start.sh --build --no-cache

if [[ "${1}" == "--build" ]];then
  docker-compose -f ../compose/dev.docker-compose.yml down -v
  docker-compose -f ../compose/dev.docker-compose.yml build --parallel "$2" benjamin-mathias-api-sender mongodb mongo-express
fi

docker-compose -f ../compose/dev.docker-compose.yml up
