#!/usr/bin/env bash
# Usage : source ./az-load-env.sh
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}" || exit

az account show 2> /dev/null
retVal=$?
if [[ ! $retVal -eq 0 ]];then
  echo -e "\n###### az-login.sh \n"
  ./1-az-login.sh
fi

source ./az-load-env.sh
az aks get-credentials --resource-group "${RESOURCE_GROUP}" --name "${AKS_CLUSTER_NAME}"
