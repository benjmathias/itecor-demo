#!/usr/bin/env bash

export NO_ACR_ATTACH=true

az account show 2> /dev/null
retVal=$?
if [[ ! $retVal -eq 0 ]];then
  echo -e "\n###### az-login.sh \n"
  ./1-az-login.sh
fi

echo -e "\n###### az-acr-init.sh \n"
./2-az-acr-init.sh
echo -e "\n###### az-aks-init.sh \n"
./3-az-aks-init.sh
