#!/usr/bin/env bash
# Usage : source ./az-load-env.sh

set -e
VARS_FILE_PATH=${VARS_FILE_PATH:-"/tmp/az_scripts_vars.env"}

echo -e "\n### Loading vars from tmp file ${VARS_FILE_PATH}"
set -o allexport

# shellcheck source=/tmp/az_scripts_vars.env
source "${VARS_FILE_PATH}"
set +o allexport
echo Success
