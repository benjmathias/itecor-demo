#!/usr/bin/env bash

# Example 1 : bash 2-az-acr-init.sh
# Example 2 : bash 2-az-acr-init.sh myContainerRegistryUniqueName myNewResourceGroup

# Optional arguments are :
# $1 => if set up, create an Azure Container Registry named from first argument, otherwise generate a name
# $2 => if set up, create a resource group named from second argument but only if no previous resource group exist
# $3 => if set up, create the resource group in location named from third argument, else use $DEFAULT_LOCATION

set -e
trap 'echo "Exiting script due to fatal error... Exit code was $?."' ERR

function get_first_az_group {
  az group list --output json | jq -r '.[-1] | .name'
}

DEFAULT_ACR="defaultAcr$(uuidgen | tr -d '-')"
DEFAULT_IMAGE=hello-world
DEFAULT_RESOURCE_GROUP=defaultgroup
DEFAULT_LOCATION="eastus"
ACR_NAME="${1:-$DEFAULT_ACR}"
AZ_LOCATION="${3:-$DEFAULT_LOCATION}"
IMAGE_TO_PULL_AND_PUSH="${4:-$DEFAULT_IMAGE}"

unset RESOURCE_GROUP
if [[ -z "${2}" ]]; then
  RESOURCE_GROUP="$(get_first_az_group)"
fi

VARS_FILE_PATH=${VARS_FILE_PATH:-"/tmp/az_scripts_vars.env"}

if [[ -z "${RESOURCE_GROUP}" ]]; then
  RESOURCE_GROUP="${2:-$DEFAULT_RESOURCE_GROUP}"
  echo -e "\n### Create a resource group ${RESOURCE_GROUP}\n"
  az group create --name "${RESOURCE_GROUP}" --location "${AZ_LOCATION}"
fi

echo "ACR_NAME=${ACR_NAME}"
echo "RESOURCE_GROUP=${RESOURCE_GROUP}"
echo -e "IMAGE_TO_PULL_AND_PUSH=${IMAGE_TO_PULL_AND_PUSH}\n"

echo -e "\n### Create an Azure Container Registry (ACR)\n"
az acr create --resource-group "${RESOURCE_GROUP}" --name "${ACR_NAME}" --sku Basic --admin-enabled true

echo -e "\n### Login to the ACR\n"
az acr login --name "${ACR_NAME}"

ACR_LOGIN_SERVER=$(az acr list --resource-group "${RESOURCE_GROUP}" --query "[].{acrLoginServer:loginServer}" \
  --output json | jq -r '.[-1] | .acrLoginServer')

echo -e "\n### Test the container registry with a pull from a known image\n"
docker pull "${IMAGE_TO_PULL_AND_PUSH}"

echo -e "\n### Test with an image push to the new Azure ACR\n"
TEST_IMAGE_ACR="${ACR_LOGIN_SERVER}"/"$(basename -- "${IMAGE_TO_PULL_AND_PUSH}")":latest
docker tag "${IMAGE_TO_PULL_AND_PUSH}" "${TEST_IMAGE_ACR}"
docker push "${TEST_IMAGE_ACR}"

echo -e "\n### List images from ACR ${ACR_NAME}\n"
az acr repository list --name "${ACR_NAME}" --output table

echo -e "\n### Deleting test image from ACR...\n"
az acr repository delete --name "${ACR_NAME}" --image "$(basename -- "${IMAGE_TO_PULL_AND_PUSH}")":latest --yes

echo -e "\n### Write var data in '${VARS_FILE_PATH}'\n"
: >"${VARS_FILE_PATH}"
echo "ACR_NAME=${ACR_NAME}" | tee -a "${VARS_FILE_PATH}"
echo "RESOURCE_GROUP=${RESOURCE_GROUP}" | tee -a "${VARS_FILE_PATH}"
echo "ACR_LOGIN_SERVER=${ACR_LOGIN_SERVER}" | tee -a "${VARS_FILE_PATH}"
