import unittest

import requests

URL = "https://datas.to/api/v1/insert"
payload = {
    "collection": "address",
    "insert": {"Name": "John", "Address": "Somewhere over the rainbow"},
}


class TestInsertRoute(unittest.TestCase):
    def test_insert_route(self):
        expected_response = {
            "Hello": "admin",
            "This payload has been inserted in mongodb database 'benjamin-mathias-db' in the collection 'address'": {
                "Name": "John",
                "Address": "Somewhere over the rainbow",
            },
        }
        res = requests.post(URL, json=payload, auth=("admin", "admin"))

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json(), expected_response)
