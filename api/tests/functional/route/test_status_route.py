import unittest

from fastapi.testclient import TestClient

from api.src.app.main import app


class TestStatusRoute(unittest.TestCase):
    def test_status_route(self):
        client = TestClient(app)

        response = client.get("/api/v1/status")
        expected_response = {
            "success": True,
            "message": "everything's fine, the sun is blue 🏖️",
            "name": "benjamin-mathias-demo",
            "status": "ok",
            "version": "x.x.x",
            "components": "",
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected_response)
