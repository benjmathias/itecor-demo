#!/bin/sh

CRON_LOG=${CRON_LOG:-"/var/log/cron.log"}
API_LOG=${API_LOG:-"/var/log/api.log"}

export "PATH=${PATH}:${HOME_benjamin-mathias}/.local/bin" >>~/.bashrc

uvicorn app.main:app --reload --host 0.0.0.0 --port "${PORT}"

# We never end the container
tail -f /dev/null
