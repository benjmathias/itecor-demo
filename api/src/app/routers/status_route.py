from fastapi import APIRouter

router = APIRouter()


@router.get("/status")
def status_route() -> dict:
    """
    route to check the status of the API and its dependencies (internal and external)\n
    @return: json with status information\n
    """
    status = "ok"
    response_status = {
        "success": True,
        "message": "everything's fine, the sun is blue 🏖️",
        "name": "benjamin-mathias-demo",
        "status": status,
        "version": "x.x.x",
        "components": "",
    }

    return response_status
