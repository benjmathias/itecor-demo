import io
import shortuuid
import requests
from fastapi import APIRouter, Depends
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorGridFSBucket
from starlette.responses import StreamingResponse
from unsplash.api import Api
from unsplash.auth import Auth

from ..helpers.services.mongodb_connect import get_database

router = APIRouter()
MAX_IMG_HEIGHT = 1080
MAX_IMG_WIDTH = 1350
IMG_DATABASE = "image-db"


@router.post("/image/search/{keyword}")
async def image_search_route(
    keyword: str,
    client: AsyncIOMotorClient = Depends(get_database),
) -> dict:
    """
    This search route let you insert some data in mongodb database from unsplash image tagged with keyword\n
    @param keyword: keyword to use for photo search\n
    @param client: Motor client object for mongodb connection\n
    @return: confirmation message in json\n
    """
    db = client[IMG_DATABASE]
    fs = AsyncIOMotorGridFSBucket(db)

    # TODO : 'Variablize' (not a security issue, these are test secrets)
    client_id = "lWQA3-WuteZ8-HEEFeu9qYVxIxqbNvB6uVBuPI7SYws"
    client_secret = "oNrYJMJGsGnxRD9htkoAe-9HSDyshI1Wx6DXYbSNG84"
    redirect_uri = "urn:ietf:wg:oauth:2.0:oob"

    auth = Auth(client_id, client_secret, redirect_uri)
    api = Api(auth)

    photos_found = api.photo.random(query=keyword)
    random_photo = photos_found[0]
    photo_id = random_photo.id

    unsplash_url = f"https://unsplash.com/photos/{photo_id}/download?force=true&w={MAX_IMG_WIDTH}&h={MAX_IMG_HEIGHT}"
    res = requests.get(unsplash_url, allow_redirects=True)
    img_name = f"{photo_id}.jpg"
    if res.status_code == 200:
        await fs.upload_from_stream(img_name, res.content)

    return {
        "success": True,
        "message": f"image named '{img_name}' from 'unsplash.com' "
        f"has been inserted in mongodb database '{IMG_DATABASE}'",
        "data": {
            "img_id": photo_id,
            "image_name": img_name,
            "mongodb_database": IMG_DATABASE,
            "unsplash_url": f"https://unsplash.com/photos/{photo_id}",
        },
    }


@router.post("/image/random")
async def image_random_route(
    client: AsyncIOMotorClient = Depends(get_database),
):
    """
    This send route let you insert some data in mongodb database through json payload\n
    @param client: Motor client object for mongodb connection\n
    @return: confirmation message in json\n
    """
    db = client[IMG_DATABASE]
    fs = AsyncIOMotorGridFSBucket(db)

    uuid = shortuuid.uuid()
    img_name = f"{uuid}.jpg"
    picsum_url = f"https://picsum.photos/seed/{uuid}/{MAX_IMG_WIDTH}/{MAX_IMG_HEIGHT}"
    res = requests.get(picsum_url, allow_redirects=True)
    if res.status_code == 200:
        await fs.upload_from_stream(img_name, res.content)

    return {
        "success": True,
        "message": f"image named '{img_name}' from 'picsum.photos' "
        f"has been inserted in mongodb database '{IMG_DATABASE}'",
        "data": {
            "img_id": uuid,
            "image_name": img_name,
            "mongodb_database": IMG_DATABASE,
            "picsum_url": picsum_url,
        },
    }


@router.get("/image/{img_id}")
async def image_route(
    img_id: str,
    client: AsyncIOMotorClient = Depends(get_database),
):
    """
    This send route let you insert some data in mongodb database through json payload\n
    @param img_id: image id useful to retrieve image from mongodb\n
    @param client: Motor client object for mongodb connection\n
    @return: confirmation message in json\n
    """
    db = client[IMG_DATABASE]
    fs = AsyncIOMotorGridFSBucket(db)

    img_name = f"{img_id}.jpg"
    grid_out = await fs.open_download_stream_by_name(img_name)
    img_bytes = await grid_out.read()

    print(type(img_bytes))
    print(dir(img_bytes))

    return StreamingResponse(io.BytesIO(img_bytes), media_type="image/png")
