import copy

from fastapi import APIRouter, Body, Depends
from motor.motor_asyncio import AsyncIOMotorClient

from ..helpers.services.http_auth_service import get_current_username
from ..helpers.services.mongodb_connect import get_database

router = APIRouter()

POST_ADDRESS_EXAMPLE = Body(
    ...,
    example={
        "collection": "address",
        "insert": {"Name": "John", "Address": "Somewhere over the rainbow"},
    },
)
MONGODB_DATABASE = "benjamin-mathias-db"


@router.post("/payload/{collection}")
async def payload_route(
    collection: str,
    payload: dict = POST_ADDRESS_EXAMPLE,
    username: str = Depends(get_current_username),
    client: AsyncIOMotorClient = Depends(get_database),
) -> dict:
    """
    This send route let you insert some data in mongodb database through json payload\n
    @param collection: mongodb collection name\n
    @param client: Motor client object for mongodb connection\n
    @param payload: input in json\n
    @param username: automatic input for basic auth\n
    @return: confirmation message in json\n
    """
    db = client[MONGODB_DATABASE]
    my_col = db[collection]
    await my_col.insert_one(copy.deepcopy(payload["insert"]))
    return {
        "success": True,
        "message": f"data has been inserted into mongodb database '{MONGODB_DATABASE}'"
        f" into '{collection}' collection",
        "data": {
            "payload": payload["insert"],
            "username": username,
            "mongodb_database": MONGODB_DATABASE,
            "mongodb_collection": collection,
        },
    }
