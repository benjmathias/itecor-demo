import configparser
import os
from functools import lru_cache
from pathlib import Path

CONFIG_FILE_PATH = Path(__file__).resolve().parents[2].joinpath("config.ini")


def retrieve_shell_env(shell_string: str):
    return os.getenv(
        shell_string.replace("{", "").replace("}", "").replace("$", ""), ""
    )


@lru_cache(maxsize=None)
def get_conf():
    """
    We retrieve the config static vars
    conf dict is as follow : "<SECTION>_<VAR_KEY>=<VAR_VALUE>"
    If value is empty or is of type env var "${}", retrieve the environment vars
    """

    def get_with_cache():
        config_path = CONFIG_FILE_PATH
        source_config = configparser.ConfigParser()
        source_config.read(config_path)
        config = {}
        for key in source_config.keys():
            for sub_key in source_config[key].keys():
                new_key = f"{key}_{sub_key.upper()}"
                if source_config[key][sub_key][0] == "$":
                    shell_string = source_config[key][sub_key]
                    var_value = retrieve_shell_env(shell_string)
                elif source_config[key][sub_key]:
                    var_value = source_config[key][sub_key]
                else:
                    var_value = os.getenv(new_key.upper(), "")
                config[new_key] = var_value
        return config

    return get_with_cache()
