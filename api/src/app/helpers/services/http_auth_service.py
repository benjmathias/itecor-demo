import secrets

from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from ..conf_handler import get_conf

security = HTTPBasic()


# Temporary Code for security reason # TEMP
def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(
        credentials.username, get_conf()["AUTH_TEMP_HTTP_USER"]
    )
    correct_password = secrets.compare_digest(
        credentials.password, get_conf()["AUTH_TEMP_HTTP_PASSWORD"]
    )
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username
