from motor.motor_asyncio import AsyncIOMotorClient

from ..conf_handler import get_conf


class DataBase:
    client: AsyncIOMotorClient = None


db = DataBase()
conf = get_conf()

MONGODB_URL = (
    f"mongodb://{conf['MONGODB_USER']}:{conf['MONGODB_PASSWORD']}@mongodb:27017/"
)


async def get_database() -> AsyncIOMotorClient:
    return db.client


async def connect_to_mongo():
    db.client = AsyncIOMotorClient(
        MONGODB_URL,
        maxPoolSize=conf["MONGODB_MAX_CONNECTIONS_COUNT"],
        minPoolSize=conf["MONGODB_MIN_CONNECTIONS_COUNT"],
    )


async def close_mongo_connection():
    db.client.close()
