# benjamin-mathias demo

Project on azure : 
- https://dev.azure.com/benjaminmathias/benjamin-mathias-CI-CD-demo

# Tech stack

- Python Back-end with fastapi
- CI/CD in Azure Devops
- Azure Kubernetes Service (AKS)
- MongoDB

# Dev steps

- 1 ⇒ Local development ✅
    - [x]  Local fastapi api "dockerized"
      ⇒ [https://gitlab.com/benjmathias/benjamin-mathias-demo/-/tree/develop/api](https://gitlab.com/benjmathias/benjamin-mathias-demo/-/tree/develop/api)
    - [x]  Local mongodb "dockerized"
      ⇒ [https://gitlab.com/benjmathias/benjamin-mathias-demo/-/tree/develop/db](https://gitlab.com/benjmathias/benjamin-mathias-demo/-/tree/develop/db)

---

- 2 ⇒ Deploy to Azure Kubernetes Service ✅
    - [x]  Create an Azure Container Registry (ACR) instance
    - [x]  Deploy to the registry
    - [x]  Create Kubernetes Cluster
    - [x]  Test connection to AKS
    - [x]  Run the containers on AKS (succes on temp IP
      ⇒ [http://20.203.129.243/docs](http://20.203.129.243/docs)) [--- screenshot](./docs/source/lens_aks_azure.png)
    - [x]  Create a static IP for "api sender" load balancer ⇒ 20.203.131.129
    - [x]  Use a DNS ⇒ http://datas.to/docs [--- screenshot](./docs/source/dns_cloudflare_static_ip_azure.png)

---

- 3 ⇒ Automate build & deployment with Azure Jobs ✅
    - [x]  Job to build and push to ACR
    - [x]  Job to deploy to AKS ⇒ apply change from Git source to the image then update
      Pods [--- screenshot](./docs/source/job_deploy_aks.png)

---

- 4⇒ Test jobs with Azure Jobs ✅
    - [x]  Unit test job [--- screenshot](./docs/source/test_job_azure.png)
    - [x]  Functional test job
    - [x]  Integration test job [--- screenshot](./docs/source/integration_test_pipeline.png)

---

- 5 ⇒ Quality & Security
    - [x]  Secrets "variablized" ⇒ Use configmap and secret k8s resource for mongodb credentials
    - [x]  Linter job for Python (github super linter image)
    - [ ]  Specific user for image (don't use root)
    - [ ]  Coverity for python ?
    - [ ]  K8s with resiliency and scalability
    - [ ]  Performance test job (big data payload
      +[https://betterprogramming.pub/python-fastapi-kubernetes-gcp-296e0dc3abb6](https://betterprogramming.pub/python-fastapi-kubernetes-gcp-296e0dc3abb6))
    - [ ]  Optimize Python and transfer Speed ⇒ Pypy ? ujson ? gzip or brotli middleware
    - [ ]  Clair check job for Python and Linux packages (docker scan as alternative or complement ?)
    - [ ]  Check for XSS vulnerability
    - [ ]  resource management
      ⇒ [https://docs.microsoft.com/en-us/azure/aks/developer-best-practices-resource-management](https://docs.microsoft.com/en-us/azure/aks/developer-best-practices-resource-management)
    - [ ]  Sonarqube job

---

- 6 ⇒ Go further
    - [x]  Use motor for concurrency with asyncio (better performance) ⇒ https://github.com/mongodb/motor/
    - [x]  Exemple in automatic openapi documentation
    - [x]  Local mongo express front to debug [compose_file](./compose/dev.docker-compose.yml) (use ./scripts/quick-start.sh)
    - [ ]  Add some routes with more complex use cases
    - [ ]  Azure service bus
    - [ ]  Fastapi Models
    - [ ]  Add a "receiver" API container
    - [ ]  Configure logs properly
    - [ ]  Image in mongodb & route that shows
      image [https://stackoverflow.com/questions/55873174/how-do-i-return-an-image-in-fastapi](https://stackoverflow.com/questions/55873174/how-do-i-return-an-image-in-fastapi)
    - [ ]  Use Atlas in Azure (keep mongodb container as backup during transfer)
    - [ ]  Use motor ⇒ [https://github.com/mongodb/motor/](https://github.com/mongodb/motor/)
    - [ ]  Airflow (from helm) use case example
    - [ ]  Structure Mongodb database
    - [ ]  Optimize Mongodb with indexes
    - [ ]  Use properly Azure monitoring
    - [ ]  Performance test with multi users simulation ⇒
    - [ ]  GraphQL subscription for instant trigger
    - [ ]  Oauth2-proxy helm
    - [ ]  Azure Data Factory & Airflow
      ⇒ [https://www.astronomer.io/guides/airflow-azure-data-factory-integration](https://www.astronomer.io/guides/airflow-azure-data-factory-integration)
    - [ ]  Kafka helm
    - [ ]  Use cachier with mongodb
    - [ ]  show kubernetes infos from the api (status route) with
      ⇒ [https://github.com/kubernetes-client/python](https://github.com/kubernetes-client/python)
    - [ ]  Kubernetes dashboard available at datas.to/dashboard
    - [ ]  Use Helm to deploy in azure pipeline

---

- 🛠️ To fix
    - [x]  Add /api as prefix to all routes
    - [x]  Use the term "collection" instead of "column" in API for mongodb insert
    - [ ]  Follow strictly RESTful best practice
      ⇒ [https://florimond.dev/en/posts/2018/08/restful-api-design-13-best-practices-to-make-your-users-happy/](https://florimond.dev/en/posts/2018/08/restful-api-design-13-best-practices-to-make-your-users-happy/)
    - [ ]  Follow K8s prod recommendations
      ⇒ [https://learnk8s.io/production-best-practices#application-development](https://learnk8s.io/production-best-practices#application-development)

---

# Usage

!!! **The API could not be up at the time you are testing**  

**"datas.to"** is a personal domain name that redirects to the static ip configured in AKS (cloudflare DNS redirection
to 52.191.234.202).  
Access the **interactive auto generated doc** with :  [**https://datas.to/docs**](https://datas.to/docs)  


### Dev usage (local docker compose) :

**Quick start : **
```bash
# Prerequesites : docker and docker-compose installed
git clone git@gitlab.com:benjmathias/fastapi-mongodb-aks-demo.git
cd benjamin-mathias-demo/scripts
bash quick-start.sh
```

Access the interactive fastapi docs with : 
- http://localhost/docs

Access the mongodb front manager with : 
- http://localhost:8080


Check the scripts in ./scripts folder for automatic azure environment setup (try `bash az-classic-full.sh`).  
Try the POST request of the interactice doc to send data to the mongodb container (user=admin, pass=admin).  


# Gif show

![show](./docs/source/gif_show.gif)

# Resource

[Benjamin Mathias / benjamin-mathias-demo](https://gitlab.com/benjmathias/benjamin-mathias-demo)

[Kubernetes on Azure tutorial - Prepare an application - Azure Kubernetes Service](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-prepare-app)

[Deviniti - DevOps tools comparison: GitLab vs. Azure DevOps](https://blog.deviniti.com/agile-devops/devops-tools-comparison-gitlab-vs-azure-devops/)

[Documentation de la messagerie Azure Service Bus](https://docs.microsoft.com/fr-fr/azure/service-bus-messaging/)

[Kubernetes + Compose = Kompose](https://kompose.io/)
